﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Centro_de_adopcion
{
    public partial class Contenedor_principal : Form
    {
        private int childFormNumber = 0;

        public Contenedor_principal()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Ventana " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegistroCliente RegCli = new RegistroCliente();
            RegCli.MdiParent = this;
            RegCli.Show();
        }

        private void animalesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegistroAnimales RegAni = new RegistroAnimales();
            RegAni.MdiParent = this;
            RegAni.Show();
        }

        private void clientesToolStripMenuItem1_Click(object sender, EventArgs e)  
        {
            Historial_Personas His = new Historial_Personas();
            His.MdiParent = this;
            His.Show();
        }

        private void animalesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Historial_Animales His = new Historial_Animales();
            His.MdiParent = this;
            His.Show();
        }

        private void adoptarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Adoptar Adop = new Adoptar();
            Adop.MdiParent = this;
            Adop.Show();
            
        }

        private void darEnAdopcionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DarEnAdopcion DarEn = new DarEnAdopcion();
            DarEn.MdiParent = this;
            DarEn.Show();
        }

        private void procesoDeAdopcionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcesoAdopcion ProAdo = new ProcesoAdopcion();
            ProAdo.MdiParent = this;
            ProAdo.Show();  
        }

        private void conoceTuMascotaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Conoce_Tu_Mascota Conoce = new Conoce_Tu_Mascota();
            Conoce.MdiParent = this;
            Conoce.Show();
        }

        private void cerrarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login Log = new Login();
            Log.Show();

            this.Hide();
        }
    }
}
