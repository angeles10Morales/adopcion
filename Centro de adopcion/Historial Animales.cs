﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiCentro;

namespace Centro_de_adopcion
{
    public partial class Historial_Animales : Historial
    {
        public Historial_Animales()
        {
            InitializeComponent();
        }
        public DataSet LlenarDataGV(string tabla)
        {
            DataSet DS;

            string cmd = string.Format("SELECT * FROM " + tabla);
            DS = Utilidades.Ejecutar(cmd);

            return DS;
        }

        private void Historial_Animales_Load(object sender, EventArgs e)
        {

            dataGridView1.DataSource = LlenarDataGV("Animales").Tables[0];
        }
    }
}
