﻿namespace Centro_de_adopcion
{
    partial class Adoptar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDatos = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTelefonos = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblApellido = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.txtCasa = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNomMascota = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSeleccionarMascota = new System.Windows.Forms.Button();
            this.txtIdMascota = new System.Windows.Forms.TextBox();
            this.btnResponsable = new System.Windows.Forms.Button();
            this.txtIdResponsable = new System.Windows.Forms.TextBox();
            this.lblIdmAS = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.txtIdAdopcion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(792, 303);
            this.btnSalir.Size = new System.Drawing.Size(138, 36);
            // 
            // lblDatos
            // 
            this.lblDatos.AutoSize = true;
            this.lblDatos.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatos.Location = new System.Drawing.Point(66, 42);
            this.lblDatos.Name = "lblDatos";
            this.lblDatos.Size = new System.Drawing.Size(160, 23);
            this.lblDatos.TabIndex = 1;
            this.lblDatos.Text = "Datos personales";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(789, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Casa:";
            // 
            // lblTelefonos
            // 
            this.lblTelefonos.AutoSize = true;
            this.lblTelefonos.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefonos.Location = new System.Drawing.Point(788, 26);
            this.lblTelefonos.Name = "lblTelefonos";
            this.lblTelefonos.Size = new System.Drawing.Size(98, 23);
            this.lblTelefonos.TabIndex = 3;
            this.lblTelefonos.Text = "Telefonos";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(789, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 18);
            this.label4.TabIndex = 5;
            this.label4.Text = "Celular:";
            // 
            // lblApellido
            // 
            this.lblApellido.AutoSize = true;
            this.lblApellido.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellido.Location = new System.Drawing.Point(38, 172);
            this.lblApellido.Name = "lblApellido";
            this.lblApellido.Size = new System.Drawing.Size(207, 18);
            this.lblApellido.TabIndex = 7;
            this.lblApellido.Text = "Apellido(s) del responsable:";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(38, 99);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(203, 18);
            this.lblNombre.TabIndex = 8;
            this.lblNombre.Text = "Nombre(s) del responsable:";
            // 
            // btnEnviar
            // 
            this.btnEnviar.Location = new System.Drawing.Point(792, 259);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(138, 38);
            this.btnEnviar.TabIndex = 10;
            this.btnEnviar.Text = "Enviar";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(41, 134);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(277, 20);
            this.txtNombre.TabIndex = 11;
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(41, 195);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(277, 20);
            this.txtApellido.TabIndex = 12;
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(792, 188);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(190, 20);
            this.txtCelular.TabIndex = 13;
            // 
            // txtCasa
            // 
            this.txtCasa.Location = new System.Drawing.Point(792, 116);
            this.txtCasa.Name = "txtCasa";
            this.txtCasa.Size = new System.Drawing.Size(190, 20);
            this.txtCasa.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(449, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 23);
            this.label2.TabIndex = 17;
            this.label2.Text = "Datos Mascota";
            // 
            // txtNomMascota
            // 
            this.txtNomMascota.Location = new System.Drawing.Point(443, 134);
            this.txtNomMascota.Name = "txtNomMascota";
            this.txtNomMascota.Size = new System.Drawing.Size(277, 20);
            this.txtNomMascota.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(440, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 18);
            this.label3.TabIndex = 18;
            this.label3.Text = "Nombre Mascota:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // btnSeleccionarMascota
            // 
            this.btnSeleccionarMascota.Location = new System.Drawing.Point(442, 239);
            this.btnSeleccionarMascota.Name = "btnSeleccionarMascota";
            this.btnSeleccionarMascota.Size = new System.Drawing.Size(207, 23);
            this.btnSeleccionarMascota.TabIndex = 20;
            this.btnSeleccionarMascota.Text = "Seleccionar Mascota";
            this.btnSeleccionarMascota.UseVisualStyleBackColor = true;
            this.btnSeleccionarMascota.Click += new System.EventHandler(this.btnSeleccionarMascota_Click);
            // 
            // txtIdMascota
            // 
            this.txtIdMascota.Location = new System.Drawing.Point(441, 199);
            this.txtIdMascota.Name = "txtIdMascota";
            this.txtIdMascota.Size = new System.Drawing.Size(45, 20);
            this.txtIdMascota.TabIndex = 21;
            // 
            // btnResponsable
            // 
            this.btnResponsable.Location = new System.Drawing.Point(41, 316);
            this.btnResponsable.Name = "btnResponsable";
            this.btnResponsable.Size = new System.Drawing.Size(207, 23);
            this.btnResponsable.TabIndex = 22;
            this.btnResponsable.Text = "Seleccionar Responsable";
            this.btnResponsable.UseVisualStyleBackColor = true;
            this.btnResponsable.Click += new System.EventHandler(this.btnResponsable_Click);
            // 
            // txtIdResponsable
            // 
            this.txtIdResponsable.Location = new System.Drawing.Point(41, 278);
            this.txtIdResponsable.Name = "txtIdResponsable";
            this.txtIdResponsable.Size = new System.Drawing.Size(45, 20);
            this.txtIdResponsable.TabIndex = 23;
            // 
            // lblIdmAS
            // 
            this.lblIdmAS.AutoSize = true;
            this.lblIdmAS.Location = new System.Drawing.Point(439, 172);
            this.lblIdmAS.Name = "lblIdmAS";
            this.lblIdmAS.Size = new System.Drawing.Size(65, 13);
            this.lblIdmAS.TabIndex = 24;
            this.lblIdmAS.Text = "ID Mascota;";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 249);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "ID Persona;";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(172, 362);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(743, 163);
            this.dataGridView1.TabIndex = 26;
            // 
            // txtIdAdopcion
            // 
            this.txtIdAdopcion.Enabled = false;
            this.txtIdAdopcion.Location = new System.Drawing.Point(412, 319);
            this.txtIdAdopcion.Name = "txtIdAdopcion";
            this.txtIdAdopcion.Size = new System.Drawing.Size(277, 20);
            this.txtIdAdopcion.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(409, 289);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 18);
            this.label6.TabIndex = 28;
            this.label6.Text = "ID Adopcion:";
            // 
            // Adoptar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 558);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtIdAdopcion);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblIdmAS);
            this.Controls.Add(this.txtIdResponsable);
            this.Controls.Add(this.btnResponsable);
            this.Controls.Add(this.txtIdMascota);
            this.Controls.Add(this.btnSeleccionarMascota);
            this.Controls.Add(this.txtNomMascota);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCasa);
            this.Controls.Add(this.txtCelular);
            this.Controls.Add(this.txtApellido);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.btnEnviar);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblApellido);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblTelefonos);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblDatos);
            this.Name = "Adoptar";
            this.Text = "Adoptar";
            this.Load += new System.EventHandler(this.Adoptar_Load);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.lblDatos, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lblTelefonos, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.lblApellido, 0);
            this.Controls.SetChildIndex(this.lblNombre, 0);
            this.Controls.SetChildIndex(this.btnEnviar, 0);
            this.Controls.SetChildIndex(this.txtNombre, 0);
            this.Controls.SetChildIndex(this.txtApellido, 0);
            this.Controls.SetChildIndex(this.txtCelular, 0);
            this.Controls.SetChildIndex(this.txtCasa, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.txtNomMascota, 0);
            this.Controls.SetChildIndex(this.btnSeleccionarMascota, 0);
            this.Controls.SetChildIndex(this.txtIdMascota, 0);
            this.Controls.SetChildIndex(this.btnResponsable, 0);
            this.Controls.SetChildIndex(this.txtIdResponsable, 0);
            this.Controls.SetChildIndex(this.lblIdmAS, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            this.Controls.SetChildIndex(this.txtIdAdopcion, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDatos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTelefonos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblApellido;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.TextBox txtCelular;
        private System.Windows.Forms.TextBox txtCasa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNomMascota;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSeleccionarMascota;
        private System.Windows.Forms.TextBox txtIdMascota;
        private System.Windows.Forms.Button btnResponsable;
        private System.Windows.Forms.TextBox txtIdResponsable;
        private System.Windows.Forms.Label lblIdmAS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtIdAdopcion;
        private System.Windows.Forms.Label label6;
    }
}