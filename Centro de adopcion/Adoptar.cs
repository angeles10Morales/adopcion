﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiCentro;

namespace Centro_de_adopcion
{
    public partial class Adoptar : Modificar_historial
    {
        public Adoptar()
        {
            InitializeComponent();
        }

        public DataSet LlenarDataGv(String tabla)
        {



            DataSet DS;

            string cmd = string.Format("Select * From ") + tabla;

            DS = Utilidades.Ejecutar(cmd);
            return DS;
    }

        private void Adoptar_Load(object sender, EventArgs e)
        {
            txtIdAdopcion.Text = "0";
            dataGridView1.DataSource = LlenarDataGv("Adoptar").Tables[0];
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnSeleccionarMascota_Click(object sender, EventArgs e)
        {

            ConsultarAnimales ConAni = new ConsultarAnimales();
            ConAni.ShowDialog();

            if (ConAni.DialogResult == DialogResult.OK)
            {
                txtIdMascota.Text = ConAni.dataGridView1.Rows[ConAni.dataGridView1.CurrentRow.Index].Cells[4].Value.ToString();

                txtNomMascota.Text = ConAni.dataGridView1.Rows[ConAni.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();

            }

        }

        private void btnResponsable_Click(object sender, EventArgs e)
        {
            ConsultarClientes ConCli = new ConsultarClientes();
            ConCli.ShowDialog();


            if (ConCli.DialogResult == DialogResult.OK)
            {
                txtIdResponsable.Text = ConCli.dataGridView1.Rows[ConCli.dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();

                txtNombre.Text = ConCli.dataGridView1.Rows[ConCli.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();

                txtApellido.Text = ConCli.dataGridView1.Rows[ConCli.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();

            }


        }

        private void btnEnviar_Click(object sender, EventArgs e) {
            txtIdAdopcion.Text = "0";

            try
            {
                txtIdResponsable.Text = "0";
                string cmd = string.Format("EXEC adopciones '{0}','{1}','{2}','{4}','{5}','{6}','{7}'", txtApellido.Text.Trim(), txtIdAdopcion.Text.Trim(),txtIdResponsable.Text.Trim(),txtIdMascota.Text.Trim(),txtCasa.Text.Trim(),txtCelular.Text.Trim(),txtNombre.Text.Trim(),txtNomMascota.Text.Trim());
        Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha guardado correctamente");
                

            }        catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error: " + error.Message);

            }
        }
    }
}
