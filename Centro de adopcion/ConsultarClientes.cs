﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiCentro;

namespace Centro_de_adopcion
{
    public partial class ConsultarClientes : Historial
    {
        public ConsultarClientes()
        {
            InitializeComponent();
        }

        public DataSet LlenarDataGV(string tabla)
        {
            DataSet DS;

            string cmd = string.Format("SELECT * FROM " + tabla);
            DS = Utilidades.Ejecutar(cmd);

            return DS;
        }


        private void ConsultarClientes_Load(object sender, EventArgs e)
        {
             dataGridView1.DataSource = LlenarDataGV("Cliente").Tables[0];
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtNombre.Text.Trim())== false)
            {
                try
                {
                    DataSet ds;

                    string cmd = "Select * FROM cliente WHERE Nom_cli LIKE ('%" + txtNombre.Text.Trim() + "%')";

                    ds = Utilidades.Ejecutar(cmd);

                    dataGridView1.DataSource = ds.Tables[0];
                 
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error: " + error.Message);
                }
            }
        }
    }
}
