﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiCentro;

namespace Centro_de_adopcion
{
    public partial class RegistroAnimales : Registros
    {
        public RegistroAnimales()
        {
            InitializeComponent();
        }

        public override Boolean GuardarRegistro()
        {
     if(Utilidades.validarFormulario(this, errorProvider1)== false)
            {
                try
                {
                    txtIdAni.Text = "0";
                    string cmd = string.Format("EXEC ActualizaAnimales '{0}', '{1}', '{2}', '{3}', '{4}','{5}'", txtIdAni.Text.Trim(), txtNomAni.Text.Trim(), txtFecIng.Text.Trim(), txtHistorial.Text.Trim(), txtHistoria.Text.Trim(), openfileMascota.FileName);
                    Utilidades.Ejecutar(cmd);
                    MessageBox.Show("Se ha guardado correctamente");
                    return true;

                }

                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error: " + error.Message);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public override void EliminarRegistro()
        {
            try
            {
                string cmd = string.Format("EXEC EliminarAnimales '{0}'", txtIdAni.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha eliminado");

            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error" + error.Message);
            }
        }

        private void txtIdAni_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
        }

        private void RegistroAnimales_Load(object sender, EventArgs e)
        {
            txtIdAni.Text = "0";


        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
      
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            ConsultarAnimales ConAni = new ConsultarAnimales();
            ConAni.ShowDialog();

            if (ConAni.DialogResult == DialogResult.OK)
            {
                txtIdAni.Text = ConAni.dataGridView1.Rows[ConAni.dataGridView1.CurrentRow.Index].Cells[4].Value.ToString();



            }




        }

        private void btnCargarImagen_Click(object sender, EventArgs e)
        {

            if (openfileMascota.ShowDialog() == DialogResult.OK)
            {

                string Imagen = openfileMascota.FileName;
            }


            MessageBox.Show("Se Guardo La Imagen");

        }
    }
}
