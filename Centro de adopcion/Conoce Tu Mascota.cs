﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiCentro;
namespace Centro_de_adopcion
{
    public partial class Conoce_Tu_Mascota : Form
    {
        public Conoce_Tu_Mascota()
        {
            InitializeComponent();
        }

        public DataSet LlenarComboBox(string tabla)
        {
            DataSet DS;
            string cmd = string.Format("SELEcT * FROM " + tabla);

            DS = Utilidades.Ejecutar(cmd);

            return DS;
        }


        private void Conoce_Tu_Mascota_Load(object sender, EventArgs e)
        {
            cmbMascotas.DataSource = LlenarComboBox("Animales").Tables[0];
            cmbMascotas.DisplayMember = "Nom_animal";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet DS;
                string cmd = string.Format("SELECT * FROM Animales WHERE Nom_animal ='{0}'", cmbMascotas.Text);
                DS = Utilidades.Ejecutar(cmd);



                string imagen = DS.Tables[0].Rows[0]["Imagen"].ToString().Trim();
                pictureBox1.ImageLocation = imagen;

            } catch (Exception error)
            {

                MessageBox.Show("Ha Ocurrido un error", error.Message);
            }
        }
    }
}
