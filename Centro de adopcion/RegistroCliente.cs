﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiCentro;

namespace Centro_de_adopcion
{
    public partial class RegistroCliente : Registros
    {
        public RegistroCliente()
        {
            InitializeComponent();
        }
        public override Boolean GuardarRegistro()
        {
           
            try
            {
                txtIdCli.Text = "0";
                string cmd = string.Format("EXEC ActualizaCliente '{0}', '{1}', '{2}'", txtIdCli.Text.Trim(), txtNomCli.Text.Trim(), txtApeCli.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha guardado correctamente");
                return true;

            }

            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error: " + error.Message);
                return false;
            }
        }

        public override void EliminarRegistro()
        {
            try
            {
                string cmd = string.Format("EXEC EliminarClientes '{0}'", txtIdCli.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha eliminado");

            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error" + error.Message);
            }
        }

        private void RegistroCliente_Load(object sender, EventArgs e)
        {
            txtIdCli.Text = "0";

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Historial_Personas His = new Historial_Personas();
         

            His.ShowDialog();

            if (His.DialogResult == DialogResult.OK)
            {
                txtIdCli.Text = His.dataGridView1.Rows[His.dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();



            }

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {

        }
    }
}

    //private void RegistroCliente_Load(object sender, EventArgs e)

    


