﻿namespace Centro_de_adopcion
{
    partial class RegistroAnimales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAnimal = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.lblHistoria = new System.Windows.Forms.Label();
            this.lblHistorial = new System.Windows.Forms.Label();
            this.txtHistoria = new MiCentro.ErrortxtBox();
            this.txtHistorial = new MiCentro.ErrortxtBox();
            this.txtFecIng = new MiCentro.ErrortxtBox();
            this.txtNomAni = new MiCentro.ErrortxtBox();
            this.txtIdAni = new MiCentro.ErrortxtBox();
            this.btnCargarImagen = new System.Windows.Forms.Button();
            this.openfileMascota = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnConsultar
            // 
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // lblAnimal
            // 
            this.lblAnimal.AutoSize = true;
            this.lblAnimal.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnimal.Location = new System.Drawing.Point(90, 73);
            this.lblAnimal.Name = "lblAnimal";
            this.lblAnimal.Size = new System.Drawing.Size(91, 19);
            this.lblAnimal.TabIndex = 8;
            this.lblAnimal.Text = "ID_Animal:";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(113, 112);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(68, 19);
            this.lblNombre.TabIndex = 9;
            this.lblNombre.Text = "Nombre:";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.Location = new System.Drawing.Point(73, 151);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(108, 19);
            this.lblFecha.TabIndex = 10;
            this.lblFecha.Text = "Fecha Ingreso:";
            // 
            // lblHistoria
            // 
            this.lblHistoria.AutoSize = true;
            this.lblHistoria.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHistoria.Location = new System.Drawing.Point(113, 230);
            this.lblHistoria.Name = "lblHistoria";
            this.lblHistoria.Size = new System.Drawing.Size(69, 19);
            this.lblHistoria.TabIndex = 14;
            this.lblHistoria.Text = "Historia:";
            // 
            // lblHistorial
            // 
            this.lblHistorial.AutoSize = true;
            this.lblHistorial.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHistorial.Location = new System.Drawing.Point(54, 189);
            this.lblHistorial.Name = "lblHistorial";
            this.lblHistorial.Size = new System.Drawing.Size(127, 19);
            this.lblHistorial.TabIndex = 13;
            this.lblHistorial.Text = "Historial medico:";
            // 
            // txtHistoria
            // 
            this.txtHistoria.Location = new System.Drawing.Point(213, 227);
            this.txtHistoria.Name = "txtHistoria";
            this.txtHistoria.Size = new System.Drawing.Size(150, 20);
            this.txtHistoria.SoloNumeros = false;
            this.txtHistoria.TabIndex = 19;
            this.txtHistoria.Validar = true;
            // 
            // txtHistorial
            // 
            this.txtHistorial.Location = new System.Drawing.Point(213, 188);
            this.txtHistorial.Name = "txtHistorial";
            this.txtHistorial.Size = new System.Drawing.Size(150, 20);
            this.txtHistorial.SoloNumeros = false;
            this.txtHistorial.TabIndex = 18;
            this.txtHistorial.Validar = true;
            // 
            // txtFecIng
            // 
            this.txtFecIng.Location = new System.Drawing.Point(213, 150);
            this.txtFecIng.Name = "txtFecIng";
            this.txtFecIng.Size = new System.Drawing.Size(150, 20);
            this.txtFecIng.SoloNumeros = false;
            this.txtFecIng.TabIndex = 17;
            this.txtFecIng.Validar = true;
            // 
            // txtNomAni
            // 
            this.txtNomAni.Location = new System.Drawing.Point(213, 111);
            this.txtNomAni.Name = "txtNomAni";
            this.txtNomAni.Size = new System.Drawing.Size(150, 20);
            this.txtNomAni.SoloNumeros = false;
            this.txtNomAni.TabIndex = 16;
            this.txtNomAni.Validar = true;
            // 
            // txtIdAni
            // 
            this.txtIdAni.Enabled = false;
            this.txtIdAni.Location = new System.Drawing.Point(213, 72);
            this.txtIdAni.Name = "txtIdAni";
            this.txtIdAni.Size = new System.Drawing.Size(150, 20);
            this.txtIdAni.SoloNumeros = true;
            this.txtIdAni.TabIndex = 15;
            this.txtIdAni.Validar = true;
            this.txtIdAni.TextChanged += new System.EventHandler(this.txtIdAni_TextChanged);
            // 
            // btnCargarImagen
            // 
            this.btnCargarImagen.Location = new System.Drawing.Point(117, 304);
            this.btnCargarImagen.Name = "btnCargarImagen";
            this.btnCargarImagen.Size = new System.Drawing.Size(147, 23);
            this.btnCargarImagen.TabIndex = 20;
            this.btnCargarImagen.Text = "Cargar Imagen";
            this.btnCargarImagen.UseVisualStyleBackColor = true;
            this.btnCargarImagen.Click += new System.EventHandler(this.btnCargarImagen_Click);
            // 
            // openfileMascota
            // 
            this.openfileMascota.FileName = "openFileDialog1";
            // 
            // RegistroAnimales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 402);
            this.Controls.Add(this.btnCargarImagen);
            this.Controls.Add(this.txtHistoria);
            this.Controls.Add(this.txtHistorial);
            this.Controls.Add(this.txtFecIng);
            this.Controls.Add(this.txtNomAni);
            this.Controls.Add(this.txtIdAni);
            this.Controls.Add(this.lblHistoria);
            this.Controls.Add(this.lblHistorial);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblAnimal);
            this.Name = "RegistroAnimales";
            this.Text = "RegistroAnimales";
            this.Load += new System.EventHandler(this.RegistroAnimales_Load);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.btnConsultar, 0);
            this.Controls.SetChildIndex(this.btnNuevo, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnGuardar, 0);
            this.Controls.SetChildIndex(this.lblAnimal, 0);
            this.Controls.SetChildIndex(this.lblNombre, 0);
            this.Controls.SetChildIndex(this.lblFecha, 0);
            this.Controls.SetChildIndex(this.lblHistorial, 0);
            this.Controls.SetChildIndex(this.lblHistoria, 0);
            this.Controls.SetChildIndex(this.txtIdAni, 0);
            this.Controls.SetChildIndex(this.txtNomAni, 0);
            this.Controls.SetChildIndex(this.txtFecIng, 0);
            this.Controls.SetChildIndex(this.txtHistorial, 0);
            this.Controls.SetChildIndex(this.txtHistoria, 0);
            this.Controls.SetChildIndex(this.btnCargarImagen, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblAnimal;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Label lblHistoria;
        private System.Windows.Forms.Label lblHistorial;
        private MiCentro.ErrortxtBox txtIdAni;
        private MiCentro.ErrortxtBox txtNomAni;
        private MiCentro.ErrortxtBox txtFecIng;
        private MiCentro.ErrortxtBox txtHistorial;
        private MiCentro.ErrortxtBox txtHistoria;
        private System.Windows.Forms.Button btnCargarImagen;
        private System.Windows.Forms.OpenFileDialog openfileMascota;
    }
}