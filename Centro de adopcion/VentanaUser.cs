﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiCentro;

namespace Centro_de_adopcion
{
    public partial class VentanaUser : FormBase
    {
        public VentanaUser()
        {
            InitializeComponent();
        }

        private void VentanaUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void VentanaUser_Load(object sender, EventArgs e)
        {
            string cmd = "Select * FROM Usuarios WHERE id_usuario =" + Login.Codigo;

            DataSet DS = Utilidades.Ejecutar(cmd);

            lblNomCli.Text = DS.Tables[0].Rows[0]["Nom_usu"].ToString();
            lblUsu.Text = DS.Tables[0].Rows[0]["account"].ToString();
            lblCod.Text = DS.Tables[0].Rows[0]["id_usuario"].ToString();

            string url = DS.Tables[0].Rows[0]["Foto"].ToString();

           pictureBox1.Image = Image.FromFile(url);
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            Contenedor_principal ConP = new Contenedor_principal();
            this.Hide();
            ConP.Show();
        }
    }
}
