﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiCentro;

namespace Centro_de_adopcion
{
    public partial class ConsultarAnimales : Historial
    {
        public ConsultarAnimales()
        {
            InitializeComponent();
        }
        public DataSet LlenarDataGV(string tabla)
        {
            DataSet DS;

            string cmd = string.Format("SELECT * FROM " + tabla);
            DS = Utilidades.Ejecutar(cmd);

            return DS;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNombre.Text.Trim()) == false)
            {
                try
                {
                    DataSet ds;

                    string cmd = "Select * FROM Animales WHERE id_Nom LIKE ('%" + txtNombre.Text.Trim() + "%')";

                    ds = Utilidades.Ejecutar(cmd);

                    dataGridView1.DataSource = ds.Tables[0];

                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error: " + error.Message);
                }
            }
        }

        private void ConsultarAnimales_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = LlenarDataGV("Animales").Tables[0];
        }
    }
}
 