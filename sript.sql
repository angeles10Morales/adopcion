USE [Adopcion]
GO
/****** Object:  Table [dbo].[Adoptar]    Script Date: 12/04/2019 1:50:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adoptar](
	[ApellidoRes] [varchar](50) NULL,
	[id_NomResp] [int] IDENTITY(1,1) NOT NULL,
	[id_clientes] [int] NOT NULL,
	[id_Nom] [int] NOT NULL,
	[casa] [int] NULL,
	[celular] [int] NULL,
	[NombrerRes] [nvarchar](100) NULL,
	[NomMascota] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_NomResp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Animales]    Script Date: 12/04/2019 1:50:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Animales](
	[Nom_animal] [varchar](50) NULL,
	[Fech_ingreso] [varchar](50) NULL,
	[Historia] [varchar](50) NULL,
	[Hist_medico] [varchar](50) NULL,
	[id_Nom] [int] IDENTITY(1,1) NOT NULL,
	[Imagen] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Nom] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 12/04/2019 1:50:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[Nom_cli] [varchar](50) NULL,
	[Ape_cli] [varchar](50) NULL,
	[id_clientes] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_clientes] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DarEnAdopcion]    Script Date: 12/04/2019 1:50:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DarEnAdopcion](
	[Nom_Respo] [varchar](50) NULL,
	[Especie] [varchar](50) NULL,
	[Fecha] [date] NULL,
	[id_NomAni] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_NomAni] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 12/04/2019 1:50:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[Nom_usu] [varchar](50) NULL,
	[account] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[Status_admin] [bit] NULL,
	[Foto] [varchar](500) NULL,
	[id_usuario] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Adoptar]  WITH CHECK ADD  CONSTRAINT [FK_Adoptar_Animales] FOREIGN KEY([id_Nom])
REFERENCES [dbo].[Animales] ([id_Nom])
GO
ALTER TABLE [dbo].[Adoptar] CHECK CONSTRAINT [FK_Adoptar_Animales]
GO
ALTER TABLE [dbo].[Adoptar]  WITH CHECK ADD  CONSTRAINT [FK_Adoptar_Cliente] FOREIGN KEY([id_clientes])
REFERENCES [dbo].[Cliente] ([id_clientes])
GO
ALTER TABLE [dbo].[Adoptar] CHECK CONSTRAINT [FK_Adoptar_Cliente]
GO
/****** Object:  StoredProcedure [dbo].[ActualizaAnimales]    Script Date: 12/04/2019 1:50:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ActualizaAnimales]

@id_Nom int, @Nom_animal varchar(100), @Fech_ingreso varchar (100), @Historia varchar(100), @Hist_medico varchar(100),@imagen nvarchar(200)

as

-- actualiza animales 

if not exists (SELECT id_Nom FROM Animales where id_Nom=@id_Nom) 
insert into Animales (Nom_animal,Fech_ingreso,Historia,Hist_medico,Imagen) values (@Nom_animal,@Fech_ingreso,@Historia,@Hist_medico,@imagen)

else 

update Animales set  Nom_animal=@Nom_animal, Fech_ingreso=@Fech_ingreso, Historia=@Historia, Hist_medico=@Hist_medico, Imagen=@imagen where id_Nom=@id_Nom
GO
/****** Object:  StoredProcedure [dbo].[ActualizaCliente]    Script Date: 12/04/2019 1:50:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ActualizaCliente]

@id_cli int, @Nom_cli varchar(100), @Ape_cli varchar(100)
as

-- actualiza clientes

if not exists (SELECT id_clientes FROM Cliente where id_clientes=@id_cli) 
insert into cliente (Nom_cli,Ape_cli) values (@Nom_cli,@Ape_cli)

else 

update cliente set Nom_cli=@Nom_cli, Ape_cli=@Ape_cli where id_clientes=@id_cli
GO
/****** Object:  StoredProcedure [dbo].[adopciones]    Script Date: 12/04/2019 1:50:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adopciones] 


@ApellidoRes varchar (50), @id_NomResp int, @id_clientes int, @id_Nom int, @casa int, @celular int, @NombrerRes nvarchar(100), @NomMascota nvarchar(100)
as 

-- actualiza adopciones

if not exists (select @id_NomResp from Adoptar where id_NomResp=@id_NomResp)
insert into Adoptar (ApellidoRes,id_clientes,id_Nom,casa,celular, NombrerRes, NomMascota) values (@ApellidoRes, @id_clientes,@id_Nom,@casa,@celular,@NombrerRes,@NomMascota)

else 

update Adoptar set ApellidoRes = @ApellidoRes, id_clientes=@id_clientes,id_Nom=@id_Nom,casa=@casa, celular=@celular,NombrerRes=@NombrerRes, NomMascota=@NomMascota
where id_NomResp=@id_NomResp
GO
/****** Object:  StoredProcedure [dbo].[EliminarAnimales]    Script Date: 12/04/2019 1:50:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarAnimales] 

@id_Nom int 

as

delete from Animales where id_Nom=@id_Nom
GO
/****** Object:  StoredProcedure [dbo].[EliminarClientes]    Script Date: 12/04/2019 1:50:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarClientes]

@id_cli int 

as

delete from Cliente where id_clientes=@id_cli
GO
